
package com.test.maxmilhas.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fee {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("value")
    @Expose
    private double value;
    @SerializedName("group")
    @Expose
    private String group;
    @SerializedName("passengerType")
    @Expose
    private String passengerType;
    @SerializedName("id")
    @Expose
    private long id;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Fee() {
    }

    /**
     * 
     * @param id
     * @param passengerType
     * @param value
     * @param group
     * @param type
     */
    public Fee(String type, double value, String group, String passengerType, long id) {
        super();
        this.type = type;
        this.value = value;
        this.group = group;
        this.passengerType = passengerType;
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Fee withType(String type) {
        this.type = type;
        return this;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Fee withValue(double value) {
        this.value = value;
        return this;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Fee withGroup(String group) {
        this.group = group;
        return this;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }

    public Fee withPassengerType(String passengerType) {
        this.passengerType = passengerType;
        return this;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Fee withId(long id) {
        this.id = id;
        return this;
    }

}
