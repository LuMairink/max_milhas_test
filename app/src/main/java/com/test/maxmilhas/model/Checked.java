
package com.test.maxmilhas.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Checked {

    @SerializedName("weight")
    @Expose
    private Object weight;
    @SerializedName("prices")
    @Expose
    private List<Object> prices = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Checked() {
    }

    /**
     * 
     * @param weight
     * @param prices
     */
    public Checked(Object weight, List<Object> prices) {
        super();
        this.weight = weight;
        this.prices = prices;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(Object weight) {
        this.weight = weight;
    }

    public Checked withWeight(Object weight) {
        this.weight = weight;
        return this;
    }

    public List<Object> getPrices() {
        return prices;
    }

    public void setPrices(List<Object> prices) {
        this.prices = prices;
    }

    public Checked withPrices(List<Object> prices) {
        this.prices = prices;
        return this;
    }

}
