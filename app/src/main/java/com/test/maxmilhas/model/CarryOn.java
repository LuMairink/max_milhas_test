
package com.test.maxmilhas.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarryOn {

    @SerializedName("weight")
    @Expose
    private long weight;
    @SerializedName("prices")
    @Expose
    private List<Long> prices = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CarryOn() {
    }

    /**
     * 
     * @param weight
     * @param prices
     */
    public CarryOn(long weight, List<Long> prices) {
        super();
        this.weight = weight;
        this.prices = prices;
    }

    public long getWeight() {
        return weight;
    }

    public void setWeight(long weight) {
        this.weight = weight;
    }

    public CarryOn withWeight(long weight) {
        this.weight = weight;
        return this;
    }

    public List<Long> getPrices() {
        return prices;
    }

    public void setPrices(List<Long> prices) {
        this.prices = prices;
    }

    public CarryOn withPrices(List<Long> prices) {
        this.prices = prices;
        return this;
    }

}
