
package com.test.maxmilhas.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ota {

    @SerializedName("adult")
    @Expose
    private Adult adult;
    @SerializedName("luggage")
    @Expose
    private Luggage luggage;
    @SerializedName("checkout")
    @Expose
    private Checkout checkout;
    @SerializedName("familyCode")
    @Expose
    private String familyCode;
    @SerializedName("fareTotal")
    @Expose
    private double fareTotal;
    @SerializedName("saleTotal")
    @Expose
    private Float saleTotal;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Ota() {
    }

    /**
     * 
     * @param fareTotal
     * @param saleTotal
     * @param familyCode
     * @param luggage
     * @param adult
     * @param checkout
     */
    public Ota(Adult adult, Luggage luggage, Checkout checkout, String familyCode, double fareTotal, Float saleTotal) {
        super();
        this.adult = adult;
        this.luggage = luggage;
        this.checkout = checkout;
        this.familyCode = familyCode;
        this.fareTotal = fareTotal;
        this.saleTotal = saleTotal;
    }

    public Adult getAdult() {
        return adult;
    }

    public void setAdult(Adult adult) {
        this.adult = adult;
    }

    public Ota withAdult(Adult adult) {
        this.adult = adult;
        return this;
    }

    public Luggage getLuggage() {
        return luggage;
    }

    public void setLuggage(Luggage luggage) {
        this.luggage = luggage;
    }

    public Ota withLuggage(Luggage luggage) {
        this.luggage = luggage;
        return this;
    }

    public Checkout getCheckout() {
        return checkout;
    }

    public void setCheckout(Checkout checkout) {
        this.checkout = checkout;
    }

    public Ota withCheckout(Checkout checkout) {
        this.checkout = checkout;
        return this;
    }

    public String getFamilyCode() {
        return familyCode;
    }

    public void setFamilyCode(String familyCode) {
        this.familyCode = familyCode;
    }

    public Ota withFamilyCode(String familyCode) {
        this.familyCode = familyCode;
        return this;
    }

    public double getFareTotal() {
        return fareTotal;
    }

    public void setFareTotal(double fareTotal) {
        this.fareTotal = fareTotal;
    }

    public Ota withFareTotal(double fareTotal) {
        this.fareTotal = fareTotal;
        return this;
    }

    public Float getSaleTotal() {
        return saleTotal;
    }

    public void setSaleTotal(Float saleTotal) {
        this.saleTotal = saleTotal;
    }

    public Ota withSaleTotal(Float saleTotal) {
        this.saleTotal = saleTotal;
        return this;
    }

}
