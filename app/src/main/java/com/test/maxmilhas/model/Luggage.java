
package com.test.maxmilhas.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Luggage {

    @SerializedName("carryOn")
    @Expose
    private CarryOn carryOn;
    @SerializedName("checked")
    @Expose
    private Checked checked;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Luggage() {
    }

    /**
     * 
     * @param carryOn
     * @param checked
     */
    public Luggage(CarryOn carryOn, Checked checked) {
        super();
        this.carryOn = carryOn;
        this.checked = checked;
    }

    public CarryOn getCarryOn() {
        return carryOn;
    }

    public void setCarryOn(CarryOn carryOn) {
        this.carryOn = carryOn;
    }

    public Luggage withCarryOn(CarryOn carryOn) {
        this.carryOn = carryOn;
        return this;
    }

    public Checked getChecked() {
        return checked;
    }

    public void setChecked(Checked checked) {
        this.checked = checked;
    }

    public Luggage withChecked(Checked checked) {
        this.checked = checked;
        return this;
    }

}
