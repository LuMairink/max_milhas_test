
package com.test.maxmilhas.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pricing {

    @SerializedName("ota")
    @Expose
    private Ota ota;
    @SerializedName("airlineName")
    @Expose
    private String airlineName;
    @SerializedName("isInternational")
    @Expose
    private boolean isInternational;
    @SerializedName("bestPriceAt")
    @Expose
    private String bestPriceAt;
    @SerializedName("mmBestPriceAt")
    @Expose
    private String mmBestPriceAt;
    @SerializedName("savingPercentage")
    @Expose
    private float savingPercentage;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Pricing() {
    }

    /**
     * 
     * @param airlineName
     * @param savingPercentage
     * @param mmBestPriceAt
     * @param isInternational
     * @param ota
     * @param bestPriceAt
     */
    public Pricing(Ota ota, String airlineName, boolean isInternational, String bestPriceAt, String mmBestPriceAt, long savingPercentage) {
        super();
        this.ota = ota;
        this.airlineName = airlineName;
        this.isInternational = isInternational;
        this.bestPriceAt = bestPriceAt;
        this.mmBestPriceAt = mmBestPriceAt;
        this.savingPercentage = savingPercentage;
    }

    public Ota getOta() {
        return ota;
    }

    public void setOta(Ota ota) {
        this.ota = ota;
    }

    public Pricing withOta(Ota ota) {
        this.ota = ota;
        return this;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public Pricing withAirlineName(String airlineName) {
        this.airlineName = airlineName;
        return this;
    }

    public boolean isIsInternational() {
        return isInternational;
    }

    public void setIsInternational(boolean isInternational) {
        this.isInternational = isInternational;
    }

    public Pricing withIsInternational(boolean isInternational) {
        this.isInternational = isInternational;
        return this;
    }

    public String getBestPriceAt() {
        return bestPriceAt;
    }

    public void setBestPriceAt(String bestPriceAt) {
        this.bestPriceAt = bestPriceAt;
    }

    public Pricing withBestPriceAt(String bestPriceAt) {
        this.bestPriceAt = bestPriceAt;
        return this;
    }

    public String getMmBestPriceAt() {
        return mmBestPriceAt;
    }

    public void setMmBestPriceAt(String mmBestPriceAt) {
        this.mmBestPriceAt = mmBestPriceAt;
    }

    public Pricing withMmBestPriceAt(String mmBestPriceAt) {
        this.mmBestPriceAt = mmBestPriceAt;
        return this;
    }

    public float getSavingPercentage() {
        return savingPercentage;
    }

    public void setSavingPercentage(float savingPercentage) {
        this.savingPercentage = savingPercentage;
    }

    public Pricing withSavingPercentage(long savingPercentage) {
        this.savingPercentage = savingPercentage;
        return this;
    }

}
