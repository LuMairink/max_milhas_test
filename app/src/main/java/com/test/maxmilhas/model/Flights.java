
package com.test.maxmilhas.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flights {

    @SerializedName("outbound")
    @Expose
    private List<Outbound> outbound = null;
    @SerializedName("inbound")
    @Expose
    private List<Inbound> inbound = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Flights() {
    }

    /**
     * 
     * @param outbound
     * @param inbound
     */
    public Flights(List<Outbound> outbound, List<Inbound> inbound) {
        super();
        this.outbound = outbound;
        this.inbound = inbound;
    }

    public List<Outbound> getOutbound() {
        return outbound;
    }

    public void setOutbound(List<Outbound> outbound) {
        this.outbound = outbound;
    }

    public Flights withOutbound(List<Outbound> outbound) {
        this.outbound = outbound;
        return this;
    }

    public List<Inbound> getInbound() {
        return inbound;
    }

    public void setInbound(List<Inbound> inbound) {
        this.inbound = inbound;
    }

    public Flights withInbound(List<Inbound> inbound) {
        this.inbound = inbound;
        return this;
    }


}
