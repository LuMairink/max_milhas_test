package com.test.maxmilhas.model;

import android.arch.persistence.db.SupportSQLiteQuery;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.RawQuery;

import java.util.List;

@Dao
public interface FlightDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertFlight(Flight flight);

    @RawQuery
    List<Flight> filterFlights(SupportSQLiteQuery query);

    @Query("SELECT * FROM Flight WHERE direction = 'inbound'")
    List<Flight> getInbounds();

    @Query("SELECT * FROM Flight WHERE direction = 'outbound'")
    List<Flight> getOutbounds();
}
