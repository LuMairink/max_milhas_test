package com.test.maxmilhas.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Flight.class}, version = 1, exportSchema = false)
public abstract class FlightsDatabase extends RoomDatabase {
    public abstract FlightDao daoAccess() ;
}
