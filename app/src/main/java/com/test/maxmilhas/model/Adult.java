
package com.test.maxmilhas.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Adult {

    @SerializedName("quantity")
    @Expose
    private long quantity;
    @SerializedName("fare")
    @Expose
    private double fare;
    @SerializedName("fees")
    @Expose
    private List<Fee> fees = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Adult() {
    }

    /**
     * 
     * @param fare
     * @param fees
     * @param quantity
     */
    public Adult(long quantity, double fare, List<Fee> fees) {
        super();
        this.quantity = quantity;
        this.fare = fare;
        this.fees = fees;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Adult withQuantity(long quantity) {
        this.quantity = quantity;
        return this;
    }

    public double getFare() {
        return fare;
    }

    public void setFare(double fare) {
        this.fare = fare;
    }

    public Adult withFare(double fare) {
        this.fare = fare;
        return this;
    }

    public List<Fee> getFees() {
        return fees;
    }

    public void setFees(List<Fee> fees) {
        this.fees = fees;
    }

    public Adult withFees(List<Fee> fees) {
        this.fees = fees;
        return this;
    }

}
