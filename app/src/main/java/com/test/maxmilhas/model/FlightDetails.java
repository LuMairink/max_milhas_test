
package com.test.maxmilhas.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightDetails {

    @SerializedName("stops")
    @Expose
    private long stops;
    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("otaAvailableIn")
    @Expose
    private String otaAvailableIn;
    @SerializedName("duration")
    @Expose
    private long duration;
    @SerializedName("flightNumber")
    @Expose
    private String flightNumber;
    @SerializedName("airlineTarget")
    @Expose
    private String airlineTarget;
    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("choosedTripType")
    @Expose
    private String choosedTripType;
    @SerializedName("availableIn")
    @Expose
    private String availableIn;
    @SerializedName("trips")
    @Expose
    private List<Trip> trips = null;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("cabin")
    @Expose
    private String cabin;
    @SerializedName("pricing")
    @Expose
    private Pricing pricing;
    @SerializedName("direction")
    @Expose
    private String direction;
    @SerializedName("to")
    @Expose
    private String to;

    /**
     * No args constructor for use in serialization
     *
     */
    public FlightDetails() {
    }

    /**
     *
     * @param to
     * @param cabin
     * @param availableIn
     * @param trips
     * @param departureDate
     * @param direction
     * @param flightNumber
     * @param from
     * @param pricing
     * @param id
     * @param stops
     * @param duration
     * @param arrivalDate
     * @param otaAvailableIn
     * @param airlineTarget
     * @param airline
     * @param choosedTripType
     */
    public FlightDetails(long stops, String airline, String otaAvailableIn, long duration, String flightNumber, String airlineTarget, String from, String id, String choosedTripType, String availableIn, List<Trip> trips, String departureDate, String arrivalDate, String cabin, Pricing pricing, String direction, String to) {
        super();
        this.stops = stops;
        this.airline = airline;
        this.otaAvailableIn = otaAvailableIn;
        this.duration = duration;
        this.flightNumber = flightNumber;
        this.airlineTarget = airlineTarget;
        this.from = from;
        this.id = id;
        this.choosedTripType = choosedTripType;
        this.availableIn = availableIn;
        this.trips = trips;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.cabin = cabin;
        this.pricing = pricing;
        this.direction = direction;
        this.to = to;
    }

    public long getStops() {
        return stops;
    }

    public void setStops(long stops) {
        this.stops = stops;
    }

    public FlightDetails withStops(long stops) {
        this.stops = stops;
        return this;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public FlightDetails withAirline(String airline) {
        this.airline = airline;
        return this;
    }

    public String getOtaAvailableIn() {
        return otaAvailableIn;
    }

    public void setOtaAvailableIn(String otaAvailableIn) {
        this.otaAvailableIn = otaAvailableIn;
    }

    public FlightDetails withOtaAvailableIn(String otaAvailableIn) {
        this.otaAvailableIn = otaAvailableIn;
        return this;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public FlightDetails withDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public FlightDetails withFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
        return this;
    }

    public String getAirlineTarget() {
        return airlineTarget;
    }

    public void setAirlineTarget(String airlineTarget) {
        this.airlineTarget = airlineTarget;
    }

    public FlightDetails withAirlineTarget(String airlineTarget) {
        this.airlineTarget = airlineTarget;
        return this;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public FlightDetails withFrom(String from) {
        this.from = from;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FlightDetails withId(String id) {
        this.id = id;
        return this;
    }

    public String getChoosedTripType() {
        return choosedTripType;
    }

    public void setChoosedTripType(String choosedTripType) {
        this.choosedTripType = choosedTripType;
    }

    public FlightDetails withChoosedTripType(String choosedTripType) {
        this.choosedTripType = choosedTripType;
        return this;
    }

    public String getAvailableIn() {
        return availableIn;
    }

    public void setAvailableIn(String availableIn) {
        this.availableIn = availableIn;
    }

    public FlightDetails withAvailableIn(String availableIn) {
        this.availableIn = availableIn;
        return this;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public FlightDetails withTrips(List<Trip> trips) {
        this.trips = trips;
        return this;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public FlightDetails withDepartureDate(String departureDate) {
        this.departureDate = departureDate;
        return this;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public FlightDetails withArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
        return this;
    }

    public String getCabin() {
        return cabin;
    }

    public void setCabin(String cabin) {
        this.cabin = cabin;
    }

    public FlightDetails withCabin(String cabin) {
        this.cabin = cabin;
        return this;
    }

    public Pricing getPricing() {
        return pricing;
    }

    public void setPricing(Pricing pricing) {
        this.pricing = pricing;
    }

    public FlightDetails withPricing(Pricing pricing) {
        this.pricing = pricing;
        return this;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public FlightDetails withDirection(String direction) {
        this.direction = direction;
        return this;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public FlightDetails withTo(String to) {
        this.to = to;
        return this;
    }

}
