
package com.test.maxmilhas.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Trip {

    @SerializedName("layover")
    @Expose
    private long layover;
    @SerializedName("stops")
    @Expose
    private long stops;
    @SerializedName("aircraft")
    @Expose
    private String aircraft;
    @SerializedName("duration")
    @Expose
    private long duration;
    @SerializedName("carrier")
    @Expose
    private String carrier;
    @SerializedName("flightNumber")
    @Expose
    private String flightNumber;
    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("isInternational")
    @Expose
    private boolean isInternational;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("to")
    @Expose
    private String to;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Trip() {
    }

    /**
     * 
     * @param to
     * @param stops
     * @param duration
     * @param departureDate
     * @param isInternational
     * @param aircraft
     * @param carrier
     * @param arrivalDate
     * @param from
     * @param flightNumber
     * @param layover
     */
    public Trip(long layover, long stops, String aircraft, long duration, String carrier, String flightNumber, String from, boolean isInternational, String departureDate, String arrivalDate, String to) {
        super();
        this.layover = layover;
        this.stops = stops;
        this.aircraft = aircraft;
        this.duration = duration;
        this.carrier = carrier;
        this.flightNumber = flightNumber;
        this.from = from;
        this.isInternational = isInternational;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.to = to;
    }

    public long getLayover() {
        return layover;
    }

    public void setLayover(long layover) {
        this.layover = layover;
    }

    public Trip withLayover(long layover) {
        this.layover = layover;
        return this;
    }

    public long getStops() {
        return stops;
    }

    public void setStops(long stops) {
        this.stops = stops;
    }

    public Trip withStops(long stops) {
        this.stops = stops;
        return this;
    }

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public Trip withAircraft(String aircraft) {
        this.aircraft = aircraft;
        return this;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public Trip withDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public Trip withCarrier(String carrier) {
        this.carrier = carrier;
        return this;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Trip withFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
        return this;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Trip withFrom(String from) {
        this.from = from;
        return this;
    }

    public boolean isIsInternational() {
        return isInternational;
    }

    public void setIsInternational(boolean isInternational) {
        this.isInternational = isInternational;
    }

    public Trip withIsInternational(boolean isInternational) {
        this.isInternational = isInternational;
        return this;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public Trip withDepartureDate(String departureDate) {
        this.departureDate = departureDate;
        return this;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Trip withArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
        return this;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Trip withTo(String to) {
        this.to = to;
        return this;
    }

}
