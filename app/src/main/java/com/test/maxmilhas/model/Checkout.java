
package com.test.maxmilhas.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Checkout {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("target")
    @Expose
    private String target;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Checkout() {
    }

    /**
     * 
     * @param target
     * @param type
     */
    public Checkout(String type, String target) {
        super();
        this.type = type;
        this.target = target;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Checkout withType(String type) {
        this.type = type;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Checkout withTarget(String target) {
        this.target = target;
        return this;
    }

}
