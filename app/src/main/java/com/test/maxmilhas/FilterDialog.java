package com.test.maxmilhas;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;

import static com.test.maxmilhas.api.SortFilterUtil.AFTERNOON;
import static com.test.maxmilhas.api.SortFilterUtil.DAWN;
import static com.test.maxmilhas.api.SortFilterUtil.MORNING;
import static com.test.maxmilhas.api.SortFilterUtil.NIGHT;
import static com.test.maxmilhas.api.SortFilterUtil.STOP;
import static com.test.maxmilhas.api.SortFilterUtil.STRAIGHT;

public class FilterDialog extends DialogFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    public static final int NO_SORT = -1;
    public static final int SORT_LOWEST_PRICE = 0;
    public static final int SORT_HIGHEST_PRICE = 1;
    public static final int SORT_LOWEST_PRICE_TIME = 2;

    private List<String> filters = new ArrayList<>();

    private OnFilterListener listener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Light_NoTitleBar);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_filter, container, false);
        Button button = view.findViewById(R.id.filter_apply_button);
        CheckBox cbMorning = view.findViewById(R.id.filter_checkbox_morning);
        CheckBox cbAfternoon = view.findViewById(R.id.filter_checkbox_afternoon);
        CheckBox cbNight = view.findViewById(R.id.filter_checkbox_night);
        CheckBox cbDawn = view.findViewById(R.id.filter_checkbox_dawn);
        CheckBox cbStop = view.findViewById(R.id.filter_checkbox_straight);
        CheckBox cbStraight = view.findViewById(R.id.filter_checkbox_stop);

        cbMorning.setOnCheckedChangeListener(this);
        cbAfternoon.setOnCheckedChangeListener(this);
        cbNight.setOnCheckedChangeListener(this);
        cbDawn.setOnCheckedChangeListener(this);
        cbStraight.setOnCheckedChangeListener(this);
        cbStop.setOnCheckedChangeListener(this);

        Toolbar toolbar = view.findViewById(R.id.filter_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == android.R.id.home) {
                    dismiss();
                }
                return false;
            }
        });

        button.setOnClickListener(this);

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void setListener(OnFilterListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.filter_apply_button) {
            listener.applyFilter(filters);
            dismiss();
        }
    }

    @Override
    public void
    onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if (id == R.id.filter_checkbox_morning) {
            checkFilters(isChecked, MORNING);
        } else if (id == R.id.filter_checkbox_afternoon) {
            checkFilters(isChecked, AFTERNOON);
        } else if (id == R.id.filter_checkbox_night) {
            checkFilters(isChecked, NIGHT);
        } else if (id == R.id.filter_checkbox_dawn) {
            checkFilters(isChecked, DAWN);
        } else if (id == R.id.filter_checkbox_straight) {
            checkFilters(isChecked, STRAIGHT);
        } else if (id == R.id.filter_checkbox_stop) {
            checkFilters(isChecked, STOP);
        }
    }

    private void checkFilters(boolean checked, String filter) {
        if (checked) {
            if (!filters.contains(filter)) filters.add(filter);
        } else {
            filters.remove(filter);
        }
    }

    public interface OnFilterListener {
        void applyFilter(List<String> filters);
    }

}

