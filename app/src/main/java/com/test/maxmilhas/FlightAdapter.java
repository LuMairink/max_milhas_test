package com.test.maxmilhas;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.maxmilhas.model.Flight;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Locale;

public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.FlightHolder> {
    private Context context;
    private List<Flight> flights;

    public FlightAdapter(Context context, List<Flight> flights) {
        this.flights = flights;
        this.context = context;
    }

    @NonNull
    @Override
    public FlightHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new FlightHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FlightHolder viewHolder, int i) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ");

        Flight inbound = flights.get(i);
        viewHolder.orgTextView.setText(inbound.getOrg());
        viewHolder.dstTextView.setText(inbound.getDst());
        viewHolder.departureTextView.setText(formatter.parseDateTime(inbound.getDepDate()).toString("HH:mm"));
        viewHolder.arrivalTextView.setText(formatter.parseDateTime(inbound.getArrDate()).toString("HH:mm"));

        long minutes = inbound.getDuration();
        long hours = minutes / 60;
        long minutesRemaining = minutes % 60;
        String stops = hours + "h" + minutesRemaining + ", " + inbound.getStops() + " parada(s)";
        viewHolder.stopsTotalTimeTextView.setText(stops);
        viewHolder.priceTextView
                .setText(String.format(Locale.getDefault(), "R$ %s",
                        inbound.getPrice()));
    }

    @Override
    public int getItemCount() {
        return flights != null ? flights.size() : 0;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
        notifyDataSetChanged();
    }

    class FlightHolder extends RecyclerView.ViewHolder {

        TextView orgTextView;
        TextView dstTextView;
        TextView departureTextView;
        TextView arrivalTextView;
        TextView stopsTotalTimeTextView;
        TextView priceTextView;

        FlightHolder(View itemView) {
            super(itemView);
            orgTextView = itemView.findViewById(R.id.item_org);
            dstTextView = itemView.findViewById(R.id.item_dst);
            departureTextView = itemView.findViewById(R.id.item_departure);
            arrivalTextView = itemView.findViewById(R.id.item_arrival);
            stopsTotalTimeTextView = itemView.findViewById(R.id.item_stop);
            priceTextView = itemView.findViewById(R.id.item_price);
        }
    }
}

