package com.test.maxmilhas;

import android.arch.persistence.db.SimpleSQLiteQuery;
import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.common.collect.Collections2;
import com.test.maxmilhas.api.FlightsHolderApi;
import com.test.maxmilhas.model.Flight;
import com.test.maxmilhas.model.Flights;
import com.test.maxmilhas.model.FlightsDatabase;
import com.test.maxmilhas.model.Inbound;
import com.test.maxmilhas.model.Outbound;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.internal.EverythingIsNonNull;

import static com.test.maxmilhas.SorterDialog.NO_SORT;
import static com.test.maxmilhas.SorterDialog.SORT_HIGHEST_PRICE;
import static com.test.maxmilhas.SorterDialog.SORT_LOWEST_PRICE;
import static com.test.maxmilhas.SorterDialog.SORT_LOWEST_PRICE_TIME;
import static com.test.maxmilhas.api.SortFilterUtil.AFTERNOON;
import static com.test.maxmilhas.api.SortFilterUtil.DAWN;
import static com.test.maxmilhas.api.SortFilterUtil.MORNING;
import static com.test.maxmilhas.api.SortFilterUtil.NIGHT;
import static com.test.maxmilhas.api.SortFilterUtil.STOP;
import static com.test.maxmilhas.api.SortFilterUtil.STRAIGHT;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SorterDialog.OnSortListener, FilterDialog.OnFilterListener {
    private ListFragment inboundList = new ListFragment();
    private ListFragment outboundList = new ListFragment();
    private Flights flights;
    private FlightsPagerAdapter adapter;
    private ViewPager pager;
    private TabLayout tab;
    private List<Inbound> inbounds;
    private List<Outbound> outbounds;

    private SorterDialog sorterDialog = new SorterDialog();
    private FilterDialog filterDialog = new FilterDialog();

    private static final String DATABASE_NAME = "flights_db";
    private FlightsDatabase flightsDatabase;
    private int orderBy;
    private List<String> filters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        LinearLayout sortButton = findViewById(R.id.main_sort_button);
        LinearLayout filterButton = findViewById(R.id.main_filter_button);

        flightsDatabase = Room.databaseBuilder(getApplicationContext(),
                FlightsDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();

        pager = findViewById(R.id.view_pager);
        tab = findViewById(R.id.tabs);
        adapter = new FlightsPagerAdapter(getSupportFragmentManager());
        sorterDialog.setListener(this);
        filterDialog.setListener(this);

        sortButton.setOnClickListener(this);
        filterButton.setOnClickListener(this);
        setSupportActionBar(toolbar);
        setupViewPager();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://vcugj6hmt5.execute-api.us-east-1.amazonaws.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FlightsHolderApi flightsHolderApi = retrofit.create(FlightsHolderApi.class);
        Call<Flights> call = flightsHolderApi.getFlights();
        call.enqueue(new Callback<Flights>() {
            @Override
            @EverythingIsNonNull
            public void onResponse(Call<Flights> call, Response<Flights> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Erro na requisição", Toast.LENGTH_LONG).show();
                    return;
                }
                flights = response.body();
                if (flights != null) {
                    inbounds = new ArrayList<>(Collections2.filter(flights.getInbound(),
                            input -> {
                                assert input != null;
                                return input.getPricing().getOta() != null;
                            }));
                    outbounds = new ArrayList<>(Collections2.filter(flights.getOutbound(),
                            input -> {
                                assert input != null;
                                return input.getPricing().getOta() != null;
                            }));
                    final List<Flight> ob = new ArrayList<>();
                    final List<Flight> ib = new ArrayList<>();
                    Thread inThread = new Thread(() -> {
                        for (Inbound in : inbounds) {
                            Flight flight = new Flight();
                            flight.setId(in.getId());
                            flight.setOrg(in.getFrom());
                            flight.setDst(in.getTo());
                            flight.setDepDate(in.getDepartureDate());
                            flight.setArrDate(in.getArrivalDate());
                            flight.setPrice(in.getPricing().getOta().getSaleTotal());
                            flight.setDirection(in.getDirection());
                            flight.setDuration(in.getDuration());
                            flight.setStops(in.getStops());
                            flightsDatabase.daoAccess().insertFlight(flight);

                        }
                        ib.addAll(flightsDatabase.daoAccess().getInbounds());
                        runOnUiThread(() -> inboundList.setFlights(ib));
                    });

                    Thread outThread = new Thread(() -> {
                        for (Outbound out : outbounds) {
                            Flight flight = new Flight();
                            flight.setId(out.getId());
                            flight.setOrg(out.getFrom());
                            flight.setDst(out.getTo());
                            flight.setDepDate(out.getDepartureDate());
                            flight.setArrDate(out.getArrivalDate());
                            flight.setPrice(out.getPricing().getOta().getSaleTotal());
                            flight.setDirection(out.getDirection());
                            flight.setDuration(out.getDuration());
                            flight.setStops(out.getStops());
                            flightsDatabase.daoAccess().insertFlight(flight);
                        }
                        ob.addAll(flightsDatabase.daoAccess().getOutbounds());
                        runOnUiThread(() -> outboundList.setFlights(ob));
                    });

                    inThread.start();
                    outThread.start();
                }
            }

            @Override
            @EverythingIsNonNull
            public void onFailure(Call<Flights> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Erro na requisição", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager() {
        adapter.addFragment(outboundList, "Vôo de Ida");
        adapter.addFragment(inboundList, "Vôo de Volta");
        pager.setAdapter(adapter);
        tab.setupWithViewPager(pager);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.main_sort_button) {
            sorterDialog.show(getSupportFragmentManager(), "tag");
        } else if (id == R.id.main_filter_button) {
            filterDialog.show(getSupportFragmentManager(), "tag2");
        }
    }

    private void filter() {
        StringBuilder query = new StringBuilder("SELECT * FROM Flight WHERE direction = ? ");
        if (filters != null && !filters.isEmpty()) {
            if (filters.contains(MORNING) ||
                    filters.contains(AFTERNOON) ||
                    filters.contains(NIGHT) ||
                    filters.contains(DAWN)) {
                query.append(" and ( ");
                List<String> times = new ArrayList<>();
                if (filters.contains(MORNING))
                    times.add(" (strftime('%H', DATETIME(depDate)) >= '6' and strftime('%H', DATETIME(depDate)) < '12') ");
                if (filters.contains(AFTERNOON))
                    times.add(" (strftime('%H', DATETIME(depDate)) >= '12' and strftime('%H', DATETIME(depDate)) < '18') ");
                if (filters.contains(NIGHT))
                    times.add("(strftime('%H', DATETIME(depDate)) >= '18' and strftime('%H', DATETIME(depDate)) < '24') ");
                if (filters.contains(DAWN))
                    times.add("(strftime('%H', DATETIME(depDate)) >= '0' and strftime('%H', DATETIME(depDate)) < '6') ");

                for (int i = 0; i < times.size(); i++) {
                    String time = times.get(i);
                    query.append(time);

                    if (i < times.size() - 1)
                        query.append(" or ");
                }

                query.append(" ) ");
            }

            if (filters.contains(STRAIGHT) ||
                    filters.contains(STOP)) {
                query.append(" and (");
                List<String> flights = new ArrayList<>();
                if (filters.contains(STRAIGHT))
                    flights.add(" stops = 0 ");
                if (filters.contains(STOP))
                    flights.add(" stops > 0 ");

                for (int i = 0; i < flights.size(); i++) {
                    String time = flights.get(i);
                    query.append(time);

                    if (i < flights.size() - 1)
                        query.append(" or ");
                }

                query.append(" ) ");
            }
        }

        if (orderBy != NO_SORT) {
            if (orderBy == SORT_LOWEST_PRICE) {
                query.append(" ORDER BY price ");
            } else if (orderBy == SORT_HIGHEST_PRICE) {
                query.append(" ORDER BY price desc");
            } else if (orderBy == SORT_LOWEST_PRICE_TIME) {
                query.append(" ORDER BY price, duration ");
            }
        }

        SimpleSQLiteQuery queryIn = new SimpleSQLiteQuery(query.toString(), new Object[]{"inbound"});
        SimpleSQLiteQuery queryOut = new SimpleSQLiteQuery(query.toString(), new Object[]{"outbound"});
        Log.d("SQL", queryIn.getSql());
        new Thread(() -> {
            List<Flight> fin = flightsDatabase.daoAccess().filterFlights(queryIn);
            runOnUiThread(() -> inboundList.setFlights(fin));
        }).start();
        new Thread(() -> {
            List<Flight> fout = flightsDatabase.daoAccess().filterFlights(queryOut);
            runOnUiThread(() -> outboundList.setFlights(fout));
        }).start();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void setOrder(int order) {
        this.orderBy = order;
        filter();
    }


    @Override
    public void applyFilter(List<String> filters) {
        this.filters = filters;
        filter();
    }
}
