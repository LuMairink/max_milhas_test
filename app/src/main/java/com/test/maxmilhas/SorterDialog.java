package com.test.maxmilhas;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.support.v7.widget.Toolbar;

public class SorterDialog extends DialogFragment implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    public static final int NO_SORT = -1;
    public static final int SORT_LOWEST_PRICE = 0;
    public static final int SORT_HIGHEST_PRICE = 1;
    public static final int SORT_LOWEST_PRICE_TIME = 2;

    private int sortType = NO_SORT;

    private OnSortListener listener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Light_NoTitleBar);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_sort, container, false);
        Button button = view.findViewById(R.id.sort_apply_button);
        RadioGroup radioGroup = view.findViewById(R.id.sort_type_radio);
        Toolbar toolbar = view.findViewById(R.id.sort_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == android.R.id.home) {
                    dismiss();
                }
                return false;
            }
        });
        radioGroup.setOnCheckedChangeListener(this);
        button.setOnClickListener(this);

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void setListener(OnSortListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.radio_sort_lowest) {
            sortType = SORT_LOWEST_PRICE;
        } else if (checkedId == R.id.radio_sort_highest) {
            sortType = SORT_HIGHEST_PRICE;
        } else if (checkedId == R.id.radio_sort_lowest_flight) {
            sortType = SORT_LOWEST_PRICE_TIME;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.sort_apply_button) {
            if (sortType == SORT_LOWEST_PRICE) {
                listener.setOrder(SORT_LOWEST_PRICE);
            } else if (sortType == SORT_HIGHEST_PRICE) {
                listener.setOrder(SORT_HIGHEST_PRICE);
            } else if (sortType == SORT_LOWEST_PRICE_TIME) {
                listener.setOrder(SORT_LOWEST_PRICE_TIME);
            }
            dismiss();
        }
    }

    public interface OnSortListener {
        void setOrder(int orderBy);
    }

}

