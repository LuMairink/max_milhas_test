package com.test.maxmilhas.api;

import com.test.maxmilhas.model.Flights;

import retrofit2.Call;
import retrofit2.http.GET;

public interface FlightsHolderApi {
    @GET("hmg-search")
    Call<Flights> getFlights();
}
