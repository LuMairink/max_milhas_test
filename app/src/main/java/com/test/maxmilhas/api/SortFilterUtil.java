package com.test.maxmilhas.api;

public class SortFilterUtil {
    public static final String MORNING = "MORNING";
    public static final String NIGHT = "NIGHT";
    public static final String AFTERNOON = "AFTERNOON";
    public static final String DAWN = "DAWN";
    public static final String STRAIGHT = "STRAIGHT";
    public static final String STOP = "STOP";
}
