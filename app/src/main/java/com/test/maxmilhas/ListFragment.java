package com.test.maxmilhas;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.maxmilhas.model.Flight;

import java.util.List;
import java.util.Objects;

public class ListFragment extends Fragment {

    public static final String ARG_OBJECT = "object";
    private RecyclerView flightList;
    private FlightAdapter adapter;
    private List<Flight> flights;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.flights_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        flightList = view.findViewById(R.id.flight_list);
        setupRecycler();
    }

    private void setupRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        flightList.setLayoutManager(layoutManager);

        adapter = new FlightAdapter(getContext(), flights);
        flightList.setAdapter(adapter);

        flightList.addItemDecoration(
                new DividerItemDecoration(Objects.requireNonNull(getActivity()), DividerItemDecoration.VERTICAL));
    }

    public void setAdapter(FlightAdapter adapter) {
        this.adapter = adapter;
        flightList.setAdapter(adapter);
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
        adapter.setFlights(flights);
    }
}
